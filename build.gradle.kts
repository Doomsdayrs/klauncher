/*
 *  Copyright (c) 2023 Doomsdayrs
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import org.jetbrains.compose.ExperimentalComposeLibrary
import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
	kotlin("multiplatform")
	id("org.jetbrains.compose")
	kotlin("kapt")
}

group = "page.doomsdayrs.app"
version = "1.0.0"

repositories {
	google()
	mavenCentral()
	maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
}

kotlin {
	jvm {
		jvmToolchain(19)
		withJava()
	}
	sourceSets {
		val jvmMain by getting {
			dependencies {
				implementation(compose.desktop.currentOs)
				@OptIn(ExperimentalComposeLibrary::class)
				implementation(compose.material3)
				implementation(compose.ui)
				implementation(compose.animation)
				implementation(compose.animationGraphics)
				implementation(compose.foundation)

				implementation ("com.google.dagger:dagger:2.46.1")
				configurations["kapt"].dependencies.add(project.dependencies.create("com.google.dagger:dagger-compiler:2.46.1"))
			}
		}
		val jvmTest by getting
	}
}

compose.desktop {
	application {
		mainClass = "page.doomsdayrs.app.klauncher.MainKt"
		nativeDistributions {
			targetFormats(TargetFormat.Dmg, TargetFormat.Msi)
			packageName = "KLauncher"
			packageVersion = "1.0.0"
		}
	}
}

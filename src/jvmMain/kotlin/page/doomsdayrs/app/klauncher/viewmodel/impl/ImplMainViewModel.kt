/*
 *  Copyright (c) 2023 Doomsdayrs
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package page.doomsdayrs.app.klauncher.viewmodel.impl

import kotlinx.coroutines.flow.*
import page.doomsdayrs.app.klauncher.NavigationDestination
import page.doomsdayrs.app.klauncher.viewmodel.base.MainViewModel

/**
 * 26 / 06 / 2023
 */
class ImplMainViewModel : MainViewModel() {
	private val _activeView = MutableStateFlow<NavigationDestination>(NavigationDestination.Loading)
	private val isFirst = MutableStateFlow(true)

	override val activeView = isFirst.transform { isFirst ->
		if (isFirst) {
			emit(NavigationDestination.Intro)
		} else {
			emitAll(_activeView)
		}
	}.stateIn(viewModelScope, SharingStarted.Eagerly, NavigationDestination.Loading)


	override fun navigate(dest: NavigationDestination) {
		isFirst.value = false
		_activeView.value = dest
	}
}
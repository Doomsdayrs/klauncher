/*
 *  Copyright (c) 2023 Doomsdayrs
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package page.doomsdayrs.app.klauncher

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeOut
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.layout.Row
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import page.doomsdayrs.app.klauncher.di.DaggerViewModelFactory
import page.doomsdayrs.app.klauncher.ui.accounts.AccountsView
import page.doomsdayrs.app.klauncher.ui.browse.BrowseView
import page.doomsdayrs.app.klauncher.ui.instances.InstancesView
import page.doomsdayrs.app.klauncher.ui.intro.IntroView
import page.doomsdayrs.app.klauncher.ui.settings.SettingsView

@Composable
@Preview
fun App() {
	val viewModelFactory = remember { DaggerViewModelFactory.create() }
	val viewModel = remember { viewModelFactory.mainVM() }
	val activeView by viewModel.activeView.collectAsState()

	MaterialTheme {
		Row {
			AnimatedVisibility(
				activeView !is NavigationDestination.Intro
			) {
				KLauncherRail(activeView, viewModel::navigate)
			}

			when (activeView) {
				NavigationDestination.Browse -> BrowseView(viewModelFactory.browseVM())
				NavigationDestination.Accounts -> AccountsView(viewModelFactory.accountsVM())
				NavigationDestination.Instances -> InstancesView(viewModelFactory.instancesVM())
				NavigationDestination.Settings -> SettingsView(viewModelFactory.settingsVM())
				else -> {}
			}
		}

		AnimatedVisibility(
			activeView is NavigationDestination.Intro,
			exit = fadeOut()
		) {
			IntroView {
				viewModel.navigate(NavigationDestination.Instances)
			}
		}
	}
}

@Composable
fun KLauncherRail(
	activeView: NavigationDestination,
	navigate: (NavigationDestination) -> Unit
) {
	NavigationRail {
		NavigationRailItem(
			activeView is NavigationDestination.Instances,
			onClick = {
				navigate(NavigationDestination.Instances)
			},
			icon = {
				Icon(Icons.Default.List, "Instances")
			},
			label = {
				Text("Instances")
			}
		)
		NavigationRailItem(
			activeView is NavigationDestination.Browse,
			onClick = {
				navigate(NavigationDestination.Browse)
			},
			icon = {
				Icon(Icons.Default.Add, "Browse")
			},
			label = {
				Text("Browse")
			}
		)
		NavigationRailItem(
			activeView is NavigationDestination.Accounts,
			onClick = {
				navigate(NavigationDestination.Accounts)
			},
			icon = {
				Icon(Icons.Default.AccountCircle, "Accounts")
			},
			label = {
				Text("Accounts")
			}
		)
		NavigationRailItem(
			activeView is NavigationDestination.Settings,
			onClick = {
				navigate(NavigationDestination.Settings)
			},
			icon = {
				Icon(Icons.Default.Settings, "Settings")
			},
			label = {
				Text("Settings")
			}
		)
	}

}

fun main() = application {
	Window(onCloseRequest = ::exitApplication) {
		App()
	}
}
